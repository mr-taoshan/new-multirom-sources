#!/sbin/sh
base=""
if [ -d "/sdcard/media/multirom" ] ; then
    base="/sdcard/media/multirom"
elif [ -d "/sdcard/media/0/multirom" ] ; then
    base="/sdcard/media/0/multirom"
else
    echo "MultiROM folder was not found"
    exit 0
fi

/tmp/busybox chattr -R -i "$base"
rm -r "$base"
return $?
